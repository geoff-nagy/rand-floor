#pragma once

#include <stdint.h>

class Color4
{
public:
	Color4();
	Color4(uint8_t all);
	Color4(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha);
	Color4(const Color4 &color);

	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t a;
};
