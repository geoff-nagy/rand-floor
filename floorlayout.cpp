#include "floorlayout.h"
#include "block.h"
#include "hallwaygraph.h"
#include "point.h"
#include "doorway.h"
#include "wall.h"
#include "corner.h"
#include "image.h"

#include <cmath>
#include <cstdlib>
#include <vector>
#include <iostream>
using namespace std;

// - - - FloorLayout class member functions - - -

// specify the default colours
const Color4 FloorLayout::DEFAULT_BACKGROUND_COLOR(0, 0, 0, 0);
const Color4 FloorLayout::DEFAULT_HALLWAY_BLOCK_COLOR(0, 0, 255, 128);
const Color4 FloorLayout::DEFAULT_HALLWAY_INNER_CORNER_BLOCK_COLOR(0, 0, 128, 128);
const Color4 FloorLayout::DEFAULT_HALLWAY_OUTER_CORNER_BLOCK_COLOR(0, 0, 64, 128);
const Color4 FloorLayout::DEFAULT_ROOM_BLOCK_COLOR(0, 255, 0, 128);
const Color4 FloorLayout::DEFAULT_ROOM_INNER_CORNER_BLOCK_COLOR(0, 128, 0, 128);
const Color4 FloorLayout::DEFAULT_ROOM_OUTER_CORNER_BLOCK_COLOR(0, 64, 0, 128);
const Color4 FloorLayout::DEFAULT_DOORWAY_BLOCK_COLOR(128, 128, 128, 128);
const Color4 FloorLayout::DEFAULT_INTERSECTION_BLOCK_COLOR(0, 255, 255, 128);
const Color4 FloorLayout::DEFAULT_DOORWAY_COLOR(255);
const Color4 FloorLayout::DEFAULT_WALL_COLOR(255);

FloorLayout::FloorLayout(int maxSize,
						 int pixelsPerBlock,
						 int wallInnerOffset,
						 int branchLevel,
						 int stepSize,
						 int numStepsPerBranch,
						 int spacing,
						 bool createRooms,
					 	 int doorOpenProbability)
{
	// assign layout parameters
	this -> maxSize = max(32, maxSize);
	this -> pixelsPerBlock = max(4, pixelsPerBlock);
	this -> wallInnerOffset = max(0, min(wallInnerOffset, pixelsPerBlock / 2));
	this -> branchLevel = max(1, branchLevel);
	this -> stepSize = max(1, stepSize) + 2 + (spacing * 2);
	this -> numStepsPerBranch = max(1, numStepsPerBranch);
	this -> spacing = max(1, spacing);
	this -> doorOpenProbability = max(0, min(doorOpenProbability, 100));

	// assign active default colours
	backgroundColor = DEFAULT_BACKGROUND_COLOR;
	hallwayBlockColor = DEFAULT_HALLWAY_BLOCK_COLOR;
	hallwayInnerCornerBlockColor = DEFAULT_HALLWAY_INNER_CORNER_BLOCK_COLOR;
	hallwayOuterCornerBlockColor = DEFAULT_HALLWAY_OUTER_CORNER_BLOCK_COLOR;
	roomBlockColor = DEFAULT_ROOM_BLOCK_COLOR;
	roomInnerCornerBlockColor = DEFAULT_ROOM_INNER_CORNER_BLOCK_COLOR;
	roomOuterCornerBlockColor = DEFAULT_ROOM_OUTER_CORNER_BLOCK_COLOR;
	doorwayBlockColor = DEFAULT_DOORWAY_BLOCK_COLOR;
	intersectionBlockColor = DEFAULT_INTERSECTION_BLOCK_COLOR;
	doorwayColor = DEFAULT_DOORWAY_COLOR;
	wallColor = DEFAULT_WALL_COLOR;

	// create space for our floor plan
	hallways = new HallwayGraph();
	allocateOccupancyGrid();

	// construct the floor plan
	buildHallways();
	if(createRooms)
	{
		buildRooms();
	}
	buildWalls();
	findCorners();
}

FloorLayout::~FloorLayout()
{
	deallocateOccupancyGrid();
	delete hallways;
}

void FloorLayout::allocateOccupancyGrid()
{
	int i, j;

	blocks = new Block*[maxSize];
	for(i = 0; i < maxSize; ++ i)
	{
		blocks[i] = new Block[maxSize];
		for(j = 0; j < maxSize; ++ j)
		{
			blocks[i][j] = Block(Point(j, i), Block::BLOCK_TYPE_EMPTY);
		}
	}
}

void FloorLayout::deallocateOccupancyGrid()
{
	int i;

	for(i = 0; i < maxSize; ++ i)
	{
		delete[] blocks[i];
	}
	delete[] blocks;
}

void FloorLayout::buildHallways()
{
	const int MIN_MAN_DIST = 3;

	vector<HallwayGraph::HallwayEdge>::iterator k;
	vector<Block*>::iterator m;
	Point p1, p2;
	Point pos, newPos;

	int numStepLocations;
	int numPossibleDirections;
	int i, j;

	// add the initial center position
	hallways -> blocks.push_back(new Block(Point(maxSize / 2), Block::BLOCK_TYPE_HALLWAY));

	// now perform our walks, each of them starting from previous step locations
	for(i = 0; i < branchLevel; ++ i)
	{
		numStepLocations = hallways -> blocks.size();
		for(j = 0; j < numStepLocations; ++ j)
		{
			randomHallwayWalk(hallways -> blocks[j] -> pos, numStepsPerBranch, stepSize, MIN_MAN_DIST);
		}
	}

	// step 4: populate occupancy grid based on hallway graph
	for(k = hallways -> edges.begin(); k != hallways -> edges.end(); ++ k)
	{
		p1 = k -> b1 -> pos;
		p2 = k -> b2 -> pos;
		fillOccupancyGrid(p1, p2, Block::BLOCK_TYPE_HALLWAY);
	}

	// step 5: figure out where the hallway intersections are, where an intersection is defined as any point
	// where one has more than 2 hallway directions to choose from (note that this doesn't include rooms)
	for(m = hallways -> blocks.begin(); m != hallways -> blocks.end(); ++ m)
	{
		// retrieve block position
		pos = (*m) -> pos;
		numPossibleDirections = 0;

		// iterate through all four directions to see which are valid paths from where we are
		i = 0;
		while(i < 4 && numPossibleDirections < 3)
		{
			// compute a new position and make sure it's in range
			newPos = pos + (Point::getDirection((Point::Direction)i) * spacing * 2);
            if(newPos.x > 0 && newPos.x < maxSize && newPos.y > 0 && newPos.y < maxSize)
            {
				// if this is also a hallway block, then we're good so far
				if(blocks[newPos.y][newPos.x].type == Block::BLOCK_TYPE_HALLWAY)
				{
					numPossibleDirections ++;
				}
            }

            // advance to next direction
            i ++;
		}

		// this is an intersection if there are more than 2 possible paths (this includes backwards)
		if(numPossibleDirections > 2)
		{
			(*m) -> intersection = true;
			intersections.push_back(pos);
		}
	}
}

void FloorLayout::buildRooms()
{
	vector<Point> doorwayBlocks;
	vector<Block*>::iterator i;
	Point pos, dir, startPos, endPos, checkPos, doorwayPos;
	int y, x;
	int k;

	// project the hallway vertices outwards in all possible directions (4 max) to form rooms
	for(i = hallways -> blocks.begin(); i != hallways -> blocks.end(); ++ i)
	{
		// attempt to project from each of the 4 directions
		pos = (*i) -> pos;
		for(k = 0; k < 4; k ++)
		{
			// compute random room length
			dir = Point::getDirection((Point::Direction)k);
			startPos = pos + (dir * ((spacing * 2) + 1));
			endPos   = pos + (dir * ((spacing * 2) + 1 + (rand() % 3)));
			checkPos = endPos;

			// make sure room falls within occupancy grid bounds
			if(checkPos.x >= 0 && checkPos.x < maxSize && checkPos.y >= 0 && checkPos.y < maxSize &&
			   pos.x >= 1 && pos.x < maxSize - 1 && pos.y >= 1 && pos.y < maxSize - 1)
			{
				// if we're not overwriting a hallway, put a room in that space
				if(areBlocksNotOfType(startPos, endPos, 1, Block::BLOCK_TYPE_HALLWAY))
				{
					// place room
					fillOccupancyGrid(startPos, endPos, Block::BLOCK_TYPE_ROOM);

					// record the entrance and exit block as doorway blocks
					blocks[startPos.y - dir.y * (spacing + 0)][startPos.x - dir.x * (spacing + 0)].doorway = true;
					blocks[startPos.y - dir.y * (spacing + 1)][startPos.x - dir.x * (spacing + 1)].doorway = true;

					// record a doorway position
					doorwayPos = Point(round((pos.x + (dir.x * spacing) + (dir.x * 0.5)) * pixelsPerBlock),
									   round((pos.y + (dir.y * spacing) + (dir.y * 0.5)) * pixelsPerBlock));
					doorways.push_back(Doorway(doorwayPos,
											   Point(-dir.x, -dir.y),
											   pixelsPerBlock,
											   wallInnerOffset,
										   	   rand() % 100 < doorOpenProbability));
				}
			}
		}
	}

	// do a check to make sure we don't get any blocks that act as double-corners
	for(y = 1; y < maxSize - 1; ++ y)
	{
		for(x = 1; x < maxSize - 1; ++ x)
		{
			// this only occurs occasionally with room blocks
			if(blocks[y][x].type == Block::BLOCK_TYPE_ROOM)
			{
				// check if the block has empty blocks on opposite diagonal sides and room blocks for the remaining diagonal sides
				if(blocks[y + 1][x + 1].type == Block::BLOCK_TYPE_ROOM &&
				   blocks[y - 1][x - 1].type == Block::BLOCK_TYPE_ROOM &&
				   blocks[y + 1][x - 1].type == Block::BLOCK_TYPE_EMPTY &&
				   blocks[y - 1][x + 1].type == Block::BLOCK_TYPE_EMPTY)
				{
					blocks[y][x].type = Block::BLOCK_TYPE_EMPTY;
				}
				// check rotated by 90 degrees, too
				else if(blocks[y + 1][x + 1].type == Block::BLOCK_TYPE_EMPTY &&
					    blocks[y - 1][x - 1].type == Block::BLOCK_TYPE_EMPTY &&
					    blocks[y + 1][x - 1].type == Block::BLOCK_TYPE_ROOM &&
					    blocks[y - 1][x + 1].type == Block::BLOCK_TYPE_ROOM)
				{
					blocks[y][x].type = Block::BLOCK_TYPE_EMPTY;
				}
			}
		}
	}
}

void FloorLayout::buildWalls()
{
	Corner::Type cornerType;
	Corner::Orientation cornerOrientation;
	Point pos, neighbour, offset, normal, p1, p2;
	int x, y;
	int k, m;

	// iterate through all the blocks
	for(y = 1; y < maxSize - 1; ++ y)
	{
		for(x = 1; x < maxSize - 1; ++ x)
		{
			// record position in floating-point space
			pos = Point(x, y) * pixelsPerBlock;

			// if this is a non-empty block, check its surroundings for empty space
			if(blocks[y][x].type != Block::BLOCK_TYPE_EMPTY && !isCornerPiece(Point(x, y), cornerType, cornerOrientation))
			{
				// iterate through immediate neighbours (but not diagonally)
				for(k = -1; k < 2; ++ k)
				{
					for(m = -1; m < 2; ++ m)
					{
						// make sure the neighbourhood we're searching is within the valid layout floor space
						if(y + k >= 0 && y + k < maxSize && x + m >= 0 && x + m < maxSize)
						{
							// don't go diagonally
							if(!(abs(k) == abs(m)))
							{
								// if the surrounding space on the current side is empty, or we've transitioned to a different
								// non-empty block type, add a wall against that side
								if(blocks[y + k][x + m].type  == Block::BLOCK_TYPE_EMPTY ||
								   (blocks[y][x].type         != Block::BLOCK_TYPE_EMPTY &&
									blocks[y + k][x + m].type != Block::BLOCK_TYPE_EMPTY &&
									blocks[y][x].type         != blocks[y + k][x + m].type &&
									!blocks[y][x].doorway     &&
									!blocks[y + k][x + m].doorway))
								{
									// compute the position of the neighbouring block
									neighbour = Point((x + k), (y + m)) * pixelsPerBlock;
									offset = Point(m - k, k - m) * (pixelsPerBlock / 2);
									normal = Point(-m, -k);

									// compute wall vertices and add the wall
									p1 = (pos       + offset + normal * wallInnerOffset);// + (normal / 2);
									p2 = (neighbour + offset + normal * wallInnerOffset);// + (normal / 2);
									walls.push_back(Wall(Point(x, y), p1, p2));
								}
							}
						}
					}
				}
			}
			else if(blocks[y][x].type != Block::BLOCK_TYPE_EMPTY)
			{
				corners.push_back(Corner(Point(x, y), cornerType, cornerOrientation, pixelsPerBlock, wallInnerOffset));
			}
		}
	}
}

void FloorLayout::findCorners()
{
	Corner::Type cornerType;
	Corner::Orientation cornerOrientation;
	int x, y;

	// iterate through all the blocks
	for(y = 1; y < maxSize - 1; ++ y)
	{
		for(x = 1; x < maxSize - 1; ++ x)
		{
			// record position in floating-point space
			if(isCornerPiece(Point(x, y), cornerType, cornerOrientation))
			{
				blocks[y][x].cornerType = cornerType;
			}
		}
	}
}

void FloorLayout::randomHallwayWalk(const Point &start, int numSteps, int stepSize, int minManDist)
{
	const int MARGIN = spacing + 1;						// leave some empty blocks around the edge of the occupancy grid

	Point pos = start;
	Point newPos;
	bool allowedDirections[4];
	vector<int> directions;
	vector<Block*>::iterator j;
	int i, k;
	bool done;

	// where we are is an acceptable location by default
	hallways -> blocks.push_back(&blocks[start.y][start.x]);

	// continue stepping until we're stuck or we run out of steps
	i = 0;
	done = false;
	while(!done && i < numSteps)
	{
		// create an edge between our current location and where we were last
		hallways -> blocks.push_back(&blocks[pos.y][pos.x]);
		if(i > 0)
		{
			// create an edge between our current location and where we were last
			hallways -> edges.push_back(HallwayGraph::HallwayEdge(hallways -> blocks[hallways -> blocks.size() - 2],
																  hallways -> blocks[hallways -> blocks.size() - 1]));
		}

		// reconsider possible directions we can travel in---initially, consider all four directions
		for(k = 0; k < 4; ++ k)
		{
			allowedDirections[k] = true;
		}

		// determine which directions we can travel in while still meeting the manhattan distance minimum
		directions.clear();
		for(k = 0; k < 4; ++ k)
		{
			// iterate through the positions we've computed so far; for the current direction we might step, it must be the
			// case that the resulting position is actually on the map, and must also be at least minManDist Manhattan distance
			// units away from all previous positions we've computed; otherwise, the direction is discarded; this has the effect
			// that any new steps we take don't overlap previous steps, by ensuring "adequate" spacing exists between the existing
			// positions and the new one we want to create
			j = hallways -> blocks.begin();
			while(allowedDirections[k] && j != hallways -> blocks.end())
			{
				// compute new possible position
				newPos = pos + (Point::getDirection((Point::Direction)k) * stepSize);

				// if we can travel minManDist steps by going in this direction, then this direction is allowed so far
				allowedDirections[k] &= ((*j) -> pos - newPos).manhattan() >= minManDist;

				// can we go there without stepping out of bounds?
				allowedDirections[k] &= newPos.x >= MARGIN &&
										newPos.x < maxSize - MARGIN &&
										newPos.y >= MARGIN &&
										newPos.y < maxSize - MARGIN;

				// advance
				++ j;
			}

			// if we've determined that the kth direction can be taken, add it to the list of approved directions
			if(allowedDirections[k])
			{
				directions.push_back(k);
			}
		}

		// pick a random direction if one is available---it's entirely possible we've been trapped
		if(directions.size() > 0)
		{
			pos += Point::getDirection((Point::Direction)directions[rand() % (int)directions.size()]) * stepSize;
		}
		else
		{
			done = true;
		}

		// take another step (done from the new location we just computed, if indeed we computed one)
		++ i;
	}
}

bool FloorLayout::isCornerPiece(const Point &p, Corner::Type &cornerType, Corner::Orientation &cornerOrientation)
{
	// we define a corner piece as a block with exactly two non-diagonal neighbours, and at least 1 diagonal neighbour
	Block::Type type = blocks[p.y][p.x].type;
	int adjNeighbours = 0;
	int diaNeighbours = 0;

	// count non-diagonal neighbours
	adjNeighbours += (blocks[p.y + 0][p.x - 1].type == type);
	adjNeighbours += (blocks[p.y + 0][p.x + 1].type == type);
	adjNeighbours += (blocks[p.y - 1][p.x + 0].type == type);
	adjNeighbours += (blocks[p.y + 1][p.x + 0].type == type);

	// count diagonal neighbours
	diaNeighbours += (blocks[p.y - 1][p.x - 1].type == type);
	diaNeighbours += (blocks[p.y - 1][p.x + 1].type == type);
	diaNeighbours += (blocks[p.y + 1][p.x - 1].type == type);
	diaNeighbours += (blocks[p.y + 1][p.x + 1].type == type);

	// what type of corner is this?
	if(type != Block::BLOCK_TYPE_EMPTY)
	{
		// is this an outer corner?
		if((adjNeighbours == 4 && diaNeighbours == 3))
		{
			cornerType = Corner::CORNER_TYPE_OUTER;

			// now determine what direction the corner faces
			if     (blocks[p.y - 1][p.x - 1].type != type) cornerOrientation = Corner::CORNER_ORIENTATION_NE;
			else if(blocks[p.y + 1][p.x - 1].type != type) cornerOrientation = Corner::CORNER_ORIENTATION_SE;
			else if(blocks[p.y + 1][p.x + 1].type != type) cornerOrientation = Corner::CORNER_ORIENTATION_SW;
			else if(blocks[p.y - 1][p.x + 1].type != type) cornerOrientation = Corner::CORNER_ORIENTATION_NW;
		}
		// is this an inner corner?
		else if((adjNeighbours == 2 && diaNeighbours == 1) ||
				(adjNeighbours == 2 && diaNeighbours == 2) ||
				(adjNeighbours == 2 && diaNeighbours == 3))
		{
			cornerType = Corner::CORNER_TYPE_INNER;

			// now determine what direction the corner faces
			if     (blocks[p.y + 1][p.x + 0].type != type && (blocks[p.y + 0][p.x + 1].type != type)) cornerOrientation = Corner::CORNER_ORIENTATION_NE;
			else if(blocks[p.y - 1][p.x + 0].type != type && (blocks[p.y + 0][p.x + 1].type != type)) cornerOrientation = Corner::CORNER_ORIENTATION_SE;
			else if(blocks[p.y - 1][p.x + 0].type != type && (blocks[p.y + 0][p.x - 1].type != type)) cornerOrientation = Corner::CORNER_ORIENTATION_SW;
			else if(blocks[p.y + 1][p.x + 0].type != type && (blocks[p.y + 0][p.x - 1].type != type)) cornerOrientation = Corner::CORNER_ORIENTATION_NW;
		}
	}

	// block must also be non-empty
	return type != Block::BLOCK_TYPE_EMPTY && ((adjNeighbours == 2 && diaNeighbours == 1) ||
											   (adjNeighbours == 4 && diaNeighbours == 3) ||
											   (adjNeighbours == 2 && diaNeighbours == 2) ||
											   (adjNeighbours == 2 && diaNeighbours == 3));
}

bool FloorLayout::areBlocksNotOfType(const Point &p1, const Point &p2, int padding, Block::Type type)
{
	Point diff = p2 - p1;
	int i, j;

	if(diff.x > 0)
	{
		for(i = p1.y - padding; i <= p1.y + padding; ++ i)
			for(j = p1.x - padding; j <= p2.x + padding; ++ j)
				if(i >= 0 && j >= 0 && i < maxSize && j < maxSize)
					if(blocks[i][j].type == type)
						return false;
	}
	else if(diff.x < 0)
	{
		for(i = p1.y - padding; i <= p1.y + padding; ++ i)
			for(j = p2.x - padding; j <= p1.x + padding; ++ j)
				if(i >= 0 && j >= 0 && i < maxSize && j < maxSize)
					if(blocks[i][j].type == type)
						return false;
	}
	else if(diff.y > 0)
	{
		for(i = p1.y - padding; i <= p2.y + padding; ++ i)
			for(j = p1.x - padding; j <= p1.x + padding; ++ j)
				if(i >= 0 && j >= 0 && i < maxSize && j < maxSize)
					if(blocks[i][j].type == type)
						return false;
	}
	else if(diff.y < 0)
	{
		for(i = p2.y - padding; i <= p1.y + padding; ++ i)
			for(j = p1.x - padding; j <= p1.x + padding; ++ j)
				if(i >= 0 && j >= 0 && i < maxSize && j < maxSize)
					if(blocks[i][j].type == type)
						return false;
	}
	else if(diff.x == 0 && diff.y == 0)
	{
		for(i = p1.y - padding; i <= p1.y + padding; ++ i)
			for(j = p1.x - padding; j <= p1.x + padding; ++ j)
				if(i >= 0 && j >= 0 && i < maxSize && j < maxSize)
					if(blocks[i][j].type == type)
						return false;
	}

	return true;
}

void FloorLayout::fillOccupancyGrid(const Point &p1, const Point &p2, Block::Type type)
{
	Point diff = p2 - p1;
	int i, j;

	if(diff.x > 0)
	{
		for(i = p1.y - spacing; i <= p1.y + spacing; ++ i)
			for(j = p1.x - spacing; j <= p2.x + spacing; ++ j)
				if(i >= 0 && j >= 0 && i < maxSize && j < maxSize)
					blocks[i][j].type = type;
	}
	else if(diff.x < 0)
	{
		for(i = p1.y - spacing; i <= p1.y + spacing; ++ i)
			for(j = p2.x - spacing; j <= p1.x + spacing; ++ j)
				if(i >= 0 && j >= 0 && i < maxSize && j < maxSize)
					blocks[i][j].type = type;
	}
	else if(diff.y > 0)
	{
		for(i = p1.y - spacing; i <= p2.y + spacing; ++ i)
			for(j = p1.x - spacing; j <= p1.x + spacing; ++ j)
				if(i >= 0 && j >= 0 && i < maxSize && j < maxSize)
					blocks[i][j].type = type;
	}
	else if(diff.y < 0)
	{
		for(i = p2.y - spacing; i <= p1.y + spacing; ++ i)
			for(j = p1.x - spacing; j <= p1.x + spacing; ++ j)
				if(i >= 0 && j >= 0 && i < maxSize && j < maxSize)
					blocks[i][j].type = type;
	}
	else if(diff.x == 0 && diff.y == 0)
	{
		for(i = p1.y - spacing; i <= p1.y + spacing; ++ i)
			for(j = p1.x - spacing; j <= p1.x + spacing; ++ j)
				if(i >= 0 && j >= 0 && i < maxSize && j < maxSize)
					blocks[i][j].type = type;
	}
}

Image *FloorLayout::getImage()
{
	vector<Doorway>::iterator k;
	vector<Wall>::iterator m;
	vector<Corner>::iterator n;
	Color4 blockColor;
	Point pos;
	int i, j;

	// construct the image we'll draw onto
	Image *image = new Image(maxSize * pixelsPerBlock, maxSize * pixelsPerBlock);

	// fill with the background color
	image -> fill(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);

	// render occupancy grid
	for(i = 1; i < maxSize - 1; ++ i)
	{
		for(j = 1; j < maxSize - 1; ++ j)
		{
			// retrieve current block position (but make sure we draw from the block center)
			pos = Point(j * pixelsPerBlock, i * pixelsPerBlock) - Point(pixelsPerBlock / 2, pixelsPerBlock / 2);

			// first and foremost, we differentiate between hallway and room blocks
			if(blocks[i][j].type == Block::BLOCK_TYPE_HALLWAY)
			{
				// hallways have their own separate colours based on corner type
				if(blocks[i][j].cornerType == Corner::CORNER_TYPE_INNER)
					image -> rect(pos.x, pos.y, pixelsPerBlock, pixelsPerBlock, hallwayInnerCornerBlockColor.r, hallwayInnerCornerBlockColor.g, hallwayInnerCornerBlockColor.b, hallwayInnerCornerBlockColor.a);
				else if(blocks[i][j].cornerType == Corner::CORNER_TYPE_OUTER)
					image -> rect(pos.x, pos.y, pixelsPerBlock, pixelsPerBlock, hallwayOuterCornerBlockColor.r, hallwayOuterCornerBlockColor.g, hallwayOuterCornerBlockColor.b, hallwayOuterCornerBlockColor.a);
				else
					image -> rect(pos.x, pos.y, pixelsPerBlock, pixelsPerBlock, hallwayBlockColor.r, hallwayBlockColor.g, hallwayBlockColor.b, hallwayBlockColor.a);
			}
			else if(blocks[i][j].type == Block::BLOCK_TYPE_ROOM)
			{
				// rooms have their own separate colours based on corner type
				if(blocks[i][j].cornerType == Corner::CORNER_TYPE_INNER)
					image -> rect(pos.x, pos.y, pixelsPerBlock, pixelsPerBlock, roomInnerCornerBlockColor.r, roomInnerCornerBlockColor.g, roomInnerCornerBlockColor.b, roomInnerCornerBlockColor.a);
				else if(blocks[i][j].cornerType == Corner::CORNER_TYPE_OUTER)
					image -> rect(pos.x, pos.y, pixelsPerBlock, pixelsPerBlock, roomOuterCornerBlockColor.r, roomOuterCornerBlockColor.g, roomOuterCornerBlockColor.b, roomOuterCornerBlockColor.a);
				else
					image -> rect(pos.x, pos.y, pixelsPerBlock, pixelsPerBlock, roomBlockColor.r, roomBlockColor.g, roomBlockColor.b, roomBlockColor.a);
			}

			// doorway blocks might be important, and overwrite whatever colour the block was previously
			if(blocks[i][j].doorway)
			{
				image -> rect(pos.x, pos.y, pixelsPerBlock, pixelsPerBlock, doorwayBlockColor.r, doorwayBlockColor.g, doorwayBlockColor.b, doorwayBlockColor.a);
			}

			// intersection blocks are also important
			if(blocks[i][j].intersection)
			{
				image -> rect(pos.x, pos.y, pixelsPerBlock, pixelsPerBlock, intersectionBlockColor.r, intersectionBlockColor.g, intersectionBlockColor.b, intersectionBlockColor.a);
			}
		}
	}

	// render doorways
	for(k = doorways.begin(); k != doorways.end(); ++ k)
	{
		k -> draw(image, doorwayColor);
	}

	// render walls
	for(m = walls.begin(); m != walls.end(); ++ m)
	{
		m -> draw(image, wallColor);
	}

	// render corners
	for(n = corners.begin(); n != corners.end(); ++ n)
	{
		n -> draw(image, wallColor);
	}

	// ...and we're done!
	return image;
}

void FloorLayout::setBackgroundColor(const Color4 &color) { backgroundColor = color; }
void FloorLayout::setHallwayBlockColor(const Color4 &color) { hallwayBlockColor = color; }
void FloorLayout::setHallwayInnerCornerBlockColor(const Color4 &color) { hallwayInnerCornerBlockColor = color; }
void FloorLayout::setHallwayOuterCornerBlockColor(const Color4 &color) { hallwayOuterCornerBlockColor = color; }
void FloorLayout::setRoomBlockColor(const Color4 &color) { roomBlockColor = color; }
void FloorLayout::setRoomInnerCornerBlockColor(const Color4 &color) { roomInnerCornerBlockColor = color; }
void FloorLayout::setRoomOuterCornerBlockColor(const Color4 &color) { roomOuterCornerBlockColor = color; }
void FloorLayout::setDoorwayBlockColor(const Color4 &color) { doorwayBlockColor = color; }
void FloorLayout::setIntersectionBlockColor(const Color4 &color) { intersectionBlockColor = color; }
void FloorLayout::setDoorwayColor(const Color4 &color) { doorwayColor = color; }
void FloorLayout::setWallColor(const Color4 &color) { wallColor = color; }

void FloorLayout::getDoorways(vector<Doorway> &d) { d.assign(doorways.begin(), doorways.end()); }
void FloorLayout::getWalls(vector<Wall> &w) { w.assign(walls.begin(), walls.end()); }
void FloorLayout::getCorners(vector<Corner> &c) { c.assign(corners.begin(), corners.end()); }
void FloorLayout::getIntersections(vector<Point> &i) { i.assign(intersections.begin(), intersections.end()); }

// - - - Block class member functions - - -

// - - - BlockEdge class member functions - - -

FloorLayout::BlockEdge::BlockEdge(Block *b1, Block *b2)
{
	this -> b1 = b1;
	this -> b2 = b2;
}
