#include "block.h"
#include "point.h"

Block::Block()
{
	pos = Point(0, 0);
	type = BLOCK_TYPE_EMPTY;
	doorway = false;
	intersection = false;
	cornerType = Corner::CORNER_TYPE_NONE;
}

Block::Block(Point pos, Block::Type type)
{
	this -> pos = pos;
	this -> type = type;
	doorway = false;
	intersection = false;
	cornerType = Corner::CORNER_TYPE_NONE;
}
