#include "color4.h"

#include <stdint.h>

Color4::Color4()
	: r(0), g(0), b(0), a(0)
{ }

Color4::Color4(uint8_t all)
	: r(all), g(all), b(all), a(all)
{ }

Color4::Color4(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha)
	: r(red), g(green), b(blue), a(alpha)
{ }

Color4::Color4(const Color4 &color)
	: r(color.r), g(color.g), b(color.b), a(color.a)
{ }
