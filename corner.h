#pragma once

#include "point.h"
#include "color4.h"

class Image;

class Corner
{
public:
	// since walls have non-zero thickness, there is a distinction between inner
	// and outer corners
	typedef enum CORNERTYPE
	{
		CORNER_TYPE_NONE = 0,
		CORNER_TYPE_INNER,
		CORNER_TYPE_OUTER
	} Type;

	// diagonal direction of corner
	typedef enum ORIENTATION
	{
		CORNER_ORIENTATION_NE = 0,
		CORNER_ORIENTATION_SE,
		CORNER_ORIENTATION_SW,
		CORNER_ORIENTATION_NW
	} Orientation;

	Point pos;								// location index in occupancy grid
	Point v1, v2, v3;						// three vertices that define the corner line points (outer, inner, outer)
	Type type;								// either inner or outer
	Orientation orientation;				// diagonal direction of corner

	Corner(const Point &pos, Type type, Orientation orientation, int pixelsPerBlock, int wallInnerOffset);

	void draw(Image *image, const Color4 &color);
};
