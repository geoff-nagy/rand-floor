#pragma once

class Point
{
public:
	typedef enum DIRECTION
	{
		DIRECTION_NORTH = 0,
		DIRECTION_EAST,
		DIRECTION_SOUTH,
		DIRECTION_WEST
	} Direction;

	static Point getDirection(Direction dir);

	Point();
	Point(int p);
	Point(int x, int y);
	Point(const Point &point);

	const Point operator+(const Point &rhs) const;
	const Point operator-(const Point &rhs) const;
	const Point operator*(const Point &rhs) const;
	const Point operator*(int rhs) const;
	const Point operator/(const Point &rhs) const;
	const Point operator/(int rhs) const;
	const Point operator-() const;

	Point& operator+=(const Point &rhs);
	Point& operator-=(const Point &rhs);
	Point& operator*=(const Point &rhs);
	Point& operator*=(int rhs);
	Point& operator/=(const Point &rhs);
	Point& operator/=(int rhs);

	float length() const;
	int manhattan() const;

	int x;
	int y;
};
