CC = g++
CFLAGS = -Wall -Os
LIB_OBJS = block.o color4.o corner.o doorway.o floorlayout.o hallwaygraph.o image.o point.o wall.o lodepng/lodepng.o
ALL_OBJS = $(LIB_OBJS) main.o

# builds the static lib and example application
all: example

# builds the example application only
lib: librandfloor.a

example: main.o librandfloor.a
	$(CC) -o example main.o -L. -lrandfloor

librandfloor.a: $(LIB_OBJS)
	ar rcs librandfloor.a $(LIB_OBJS)

.cpp.o:
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@

clean:
	rm -f example *.o *.a

# this can be uncommented if you want to hang on to the object files
.INTERMEDIATE: $(ALL_OBJS)
