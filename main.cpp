#include "floorlayout.h"						// for building the layout
#include "image.h"								// for image output of layout

#include <cstdlib>
#include <ctime>
#include <iostream>
using namespace std;

int main(int args, char *argv[])
{
	// sizing characteristics---these are some decent default parameters
	const int MAX_LAYOUT_SIZE_BLOCKS = 60;		// layout fits into a space of MAX_LAYOUT_SIZE_BLOCKS x MAX_LAYOUT_SIZE_BLOCKS
	const int PIXELS_PER_BLOCK = 10;			// each layout block occupies PIXELS_PER_BLOCK pixels in the resulting image
	const int WALL_INNER_OFFSET = 2;			// implicit pixel thickness of walls and corners, by "pulling in" the walls
	const int BRANCH_LEVEL = 2;					// how many random walks to do for each step location
	const int STEP_SIZE = 2;					// parameter that controls lengths of corridors
	const int NUM_STEPS_PER_BRANCH = 3;			// controls number of steps per random walk
	const int SPACING = 1;						// number of blocks that will appear on either side of a random walk
	const bool CREATE_ROOMS = true;				// always create rooms---change to false for corridors only
	const int DOOR_OPEN_PROB = 50;				// 50% chance a room doorway is open (only used if CREATE_ROOMS == true)

	FloorLayout *layout;						// our floor layout instance
	Image *image;								// where we write the floor layout to
	int seed = 120;								// useful for replicating results, debugging, etc.

	// build the floor layout---see comments above for descriptions of these parameters
	srand(seed);
	layout = new FloorLayout(MAX_LAYOUT_SIZE_BLOCKS,
							 PIXELS_PER_BLOCK,
							 WALL_INNER_OFFSET,
							 BRANCH_LEVEL,
							 STEP_SIZE,
							 NUM_STEPS_PER_BRANCH,
							 SPACING,
							 CREATE_ROOMS,
						 	 DOOR_OPEN_PROB);

	// now write that result to an image and save it to a file; note that we
	// can specify different layout colours (which we do further below)
	// NOTE: cropping will only work for backgrounds with an alpha value of zero!
	image = layout -> getImage();
	image -> crop();
	image -> writeToPNG("example_default_colors_cropped.png");
	delete image;

	// change the colours of the layout to something that, e.g., Stage could use;
	// color specification is either Color4(all) or Color4(r, g, b, a)
	layout -> setBackgroundColor(Color4(0));
    layout -> setHallwayBlockColor(Color4(0));
	layout -> setHallwayInnerCornerBlockColor(Color4(0));
	layout -> setHallwayOuterCornerBlockColor(Color4(0));
    layout -> setRoomBlockColor(Color4(0));
	layout -> setRoomInnerCornerBlockColor(Color4(0));
	layout -> setRoomOuterCornerBlockColor(Color4(0));
    layout -> setDoorwayBlockColor(Color4(0));
    layout -> setIntersectionBlockColor(Color4());
    layout -> setDoorwayColor(Color4(0, 0, 0, 255));
    layout -> setWallColor(Color4(0, 0, 0, 255));

	// now save that too
	image = layout -> getImage();
	//image -> crop();					// probably don't want to crop image for Stage, but we could do it
	image -> writeToPNG("example_for_stage.png");
	delete image;

	// program ends
	return 0;
}
