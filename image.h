#pragma once

#include <stdint.h>
#include <string>

class Image
{
private:
	unsigned int width;
	unsigned int height;
	uint8_t *pixels;

public:
	static const int NUM_CHANNELS;

	Image(unsigned int size);
	Image(unsigned int width, unsigned int height);
	~Image();

	unsigned int getWidth();
	unsigned int getHeight();
	uint8_t *getPixels();

	void writeToPNG(const std::string &filename);

	bool inRange(unsigned int x, unsigned int y);

	void setPixel(unsigned int x, unsigned int y, uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha);
	void getPixel(unsigned int x, unsigned int y, uint8_t *red, uint8_t *green, uint8_t *blue, uint8_t *alpha);
	void getPixel(unsigned int x, unsigned int y, uint8_t *pixel);

	uint8_t getRed(unsigned int x, unsigned int y);
	uint8_t getGreen(unsigned int x, unsigned int y);
	uint8_t getBlue(unsigned int x, unsigned int y);
	uint8_t getAlpha(unsigned int x, unsigned int y);

	void fill(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha);
	void rect(unsigned int x, unsigned int y, unsigned int width, unsigned int height, uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha);
	void line(int x0, int y0, int x1, int y1, uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha);

	void crop();
};
