#include "image.h"

#include "lodepng/lodepng.h"

#include <stdint.h>
#include <cmath>
#include <cstring>
#include <string>
#include <iostream>
using namespace std;

const int Image::NUM_CHANNELS = 4;

Image::Image(unsigned int size)
{
	width = size;
	height = size;
	pixels = new uint8_t[width * height * NUM_CHANNELS];
	memset(pixels, 0, width * height * NUM_CHANNELS);
}

Image::Image(unsigned int width, unsigned int height)
{
	this -> width = width;
	this -> height = height;
	pixels = new uint8_t[width * height * NUM_CHANNELS];
	memset(pixels, 0, width * height * NUM_CHANNELS);
}

Image::~Image()
{
	delete[] pixels;
}

unsigned int Image::getWidth() { return width; }
unsigned int Image::getHeight() { return height; }
uint8_t *Image::getPixels() { return pixels; }

void Image::writeToPNG(const string &filename)
{
	lodepng_encode32_file(filename.c_str(), pixels, width, height);
}

bool Image::inRange(unsigned int x, unsigned int y)
{
	return x < width && y < height;
}

void Image::setPixel(unsigned int x, unsigned int y, uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha)
{
	uint8_t *ptr = &pixels[y * (width * NUM_CHANNELS) + (x * NUM_CHANNELS)];
	*ptr++ = red;
	*ptr++ = green;
	*ptr++ = blue;
	*ptr++ = alpha;
}

void Image::getPixel(unsigned int x, unsigned int y, uint8_t *red, uint8_t *green, uint8_t *blue, uint8_t *alpha)
{
	uint8_t *ptr = &pixels[y * (width * NUM_CHANNELS) + (x * NUM_CHANNELS)];
	*red = *ptr++;
	*green = *ptr++;
	*blue = *ptr++;
	*alpha = *ptr++;
}

void Image::getPixel(unsigned int x, unsigned int y, uint8_t *pixel)
{
	uint8_t *ptr = &pixels[y * (width * NUM_CHANNELS) + (x * NUM_CHANNELS)];
	memcpy(pixel, ptr, NUM_CHANNELS);
}

uint8_t Image::getRed(unsigned int x, unsigned int y)
{
	return pixels[y * (width * NUM_CHANNELS) + (x * NUM_CHANNELS) + 0];
}

uint8_t Image::getGreen(unsigned int x, unsigned int y)
{
	return pixels[y * (width * NUM_CHANNELS) + (x * NUM_CHANNELS) + 1];
}

uint8_t Image::getBlue(unsigned int x, unsigned int y)
{
	return pixels[y * (width * NUM_CHANNELS) + (x * NUM_CHANNELS) + 2];
}

uint8_t Image::getAlpha(unsigned int x, unsigned int y)
{
	return pixels[y * (width * NUM_CHANNELS) + (x * NUM_CHANNELS) + 3];
}

void Image::fill(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha)
{
	unsigned int numPixels = width * height;
	unsigned int i;
	uint8_t *ptr = pixels;

	for(i = 0; i < numPixels; ++ i)
	{
		(*ptr++) = red;
		(*ptr++) = green;
		(*ptr++) = blue;
		(*ptr++) = alpha;
	}
}

void Image::rect(unsigned int x, unsigned int y, unsigned int width, unsigned int height, uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha)
{
	unsigned int i, j;
	for(i = y; i < y + height; i ++)
	{
		for(j = x; j < x + width; j ++)
		{
			if(inRange(j, i))
			{
				setPixel(j, i, red, green, blue, alpha);
			}
		}
	}
}

// thanks, intertubes!
// adapted from: https://gist.github.com/bert/1085538
void Image::line(int x0, int y0, int x1, int y1, uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha)
{
  int dx =  abs (x1 - x0);
  int sx = x0 < x1 ? 1 : -1;
  int dy = -abs (y1 - y0);
  int sy = y0 < y1 ? 1 : -1;
  int err = dx + dy, e2; /* error value e_xy */

  for (;;){  /* loop */
    setPixel (x0,y0, red, green, blue, alpha);
    if (x0 == x1 && y0 == y1) break;
    e2 = 2 * err;
    if (e2 >= dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
    if (e2 <= dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
  }
}

void Image::crop()
{
	unsigned int minX = width;
	unsigned int maxX = 0;
	unsigned int minY = height;
	unsigned int maxY = 0;
	unsigned int x, y;
	unsigned int iy;
	unsigned int newWidth, newHeight;
	uint8_t *newPixels;
	uint8_t *ptr = pixels + 3;

	// figure out the bounds of the image
	for(y = 0; y < height; ++ y)
	{
		for(x = 0; x < width; ++ x)
		{
			// if this (alpha pixel) is non-empty, then it counts as something we don't want to crop
			if(*ptr)
			{
				minX = min(x, minX);
				maxX = max(x, maxX);
				minY = min(y, minY);
				maxY = max(y, maxY);
			}

			// advance to next alpha pixel
			ptr += 4;
		}
	}

	// allocate new pixel space that is at most as large as the existing content
	newWidth = maxX - minX + 1;
	newHeight = maxY - minY + 1;
	newPixels = new uint8_t[newWidth * newHeight * NUM_CHANNELS];
	memset(newPixels, 0, newWidth * newHeight * NUM_CHANNELS);

	// now do a row-wise copy of the cropped content into the beginning of our new pixel content
	ptr = newPixels;
	for(iy = minY; iy <= maxY; ++ iy)
	{
		memcpy(ptr, &pixels[iy * (width * NUM_CHANNELS) + (minX * NUM_CHANNELS)], newWidth * NUM_CHANNELS);
		ptr += (newWidth * NUM_CHANNELS);
	}

	// redirect to point to newly allocated image, and free old memory
	delete[] pixels;
	pixels = newPixels;

	// update image dimensions
	width = newWidth;
	height = newHeight;
}
