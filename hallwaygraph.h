#pragma once

#include <vector>

class Block;

class HallwayGraph
{
public:
	class HallwayEdge
	{
	public:
		HallwayEdge(Block *b1, Block *b2);
		Block *b1;
		Block *b2;
	};

	std::vector<HallwayEdge> edges;
	std::vector<Block*> blocks;
};
