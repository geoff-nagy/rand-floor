#pragma once

#include "point.h"
#include "color4.h"

class Image;

class Wall
{
public:
	Point pos;						// location index in occupancy grid
	Point p1;						// starting point in image/pixel space
	Point p2;						// ending point in image/pixel space

	Wall(const Point &pos, const Point &p1, const Point &p2);

	void draw(Image *image, const Color4 &color);
};
