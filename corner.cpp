#include "corner.h"
#include "point.h"
#include "image.h"
#include "color4.h"

Corner::Corner(const Point &pos, Corner::Type type, Corner::Orientation orientation, int pixelsPerBlock, int wallInnerOffset)
{
	const int WALL_LENGTH = pixelsPerBlock - wallInnerOffset;
	const int CORNER_OFFSET = (pixelsPerBlock / 2) - wallInnerOffset;

	// we need to work in world/image space when building vertex positions
	Point imagePos = pos * pixelsPerBlock;

	// assign basic corner properties
	this -> pos = pos;
	this -> type = type;
	this -> orientation = orientation;

	// compute corner vertex positions based on corner orientation
	if(orientation == CORNER_ORIENTATION_NE)
	{
		v2 = imagePos + Point(-CORNER_OFFSET, -CORNER_OFFSET) * (type == CORNER_TYPE_OUTER ? 1 : -1);
		v1 = Point(v2.x - WALL_LENGTH, v2.y);
		v3 = Point(v2.x, v2.y - WALL_LENGTH);
	}
	else if(orientation == CORNER_ORIENTATION_SE)
	{
		v2 = imagePos + Point(-CORNER_OFFSET, CORNER_OFFSET) * (type == CORNER_TYPE_OUTER ? 1 : -1);
		v1 = Point(v2.x - WALL_LENGTH, v2.y);
		v3 = Point(v2.x, v2.y + WALL_LENGTH);
	}
	else if(orientation == CORNER_ORIENTATION_SW)
	{
		v2 = imagePos + Point(CORNER_OFFSET, CORNER_OFFSET) * (type == CORNER_TYPE_OUTER ? 1 : -1);
		v1 = Point(v2.x + WALL_LENGTH, v2.y);
		v3 = Point(v2.x, v2.y + WALL_LENGTH);
	}
	else if(orientation == CORNER_ORIENTATION_NW)
	{
		v2 = imagePos + Point(CORNER_OFFSET, -CORNER_OFFSET) * (type == CORNER_TYPE_OUTER ? 1 : -1);
		v1 = Point(v2.x + WALL_LENGTH, v2.y);
		v3 = Point(v2.x, v2.y - WALL_LENGTH);
	}
}

void Corner::draw(Image *image, const Color4 &color)
{
	image -> line(v1.x, v1.y, v2.x, v2.y, color.r, color.g, color.b, color.a);
	image -> line(v2.x, v2.y, v3.x, v3.y, color.r, color.g, color.b, color.a);
}
