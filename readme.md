# RandFloor
A minimal cross-platform floor layout generator.

## Overview
This is a minimal, simple-to-use library for generating random floor layouts, complete with hallways, rooms, and doorways. At its simplest, it works by generating random walks of varying lengths to build hallways and rooms in an occupancy grid, and then generates walls and doorways around them.

See the example images [example_default_colors_cropped.png](https://gitlab.com/geoff-nagy/rand-floor/blob/master/example_default_colors_cropped.png) and [example_for_stage.png](https://gitlab.com/geoff-nagy/rand-floor/blob/master/example_for_stage.png) in the repository.

## Dependencies
RandFloor only depends on the excellent minimal [LodePNG library](http://lodev.org/lodepng/ "LodePNG library") for floor layout image output. For convenience, LodePNG is already included in this repository.

## Building
Simply type

	make

to build both the static library (**librandfloor.a**) and an example application (**example**) that demonstrates how to use RandFloor.

To build the library only, use

	make lib

## Using RandFloor
The source file **main.cpp** shows how to use RandFloor.

If you use RandFloor in your research, a citation would be appreciated! Please use the following BibTeX entry.

	@misc{nagy2018randfloor,
	      author = {Nagy, Geoff},
	      title = {RandFloor, A minimal cross-platform floor layout generator},
	      year = {2018},
	      howpublished = {\url{https://gitlab.com/geoff-nagy/rand-floor}}
	}
