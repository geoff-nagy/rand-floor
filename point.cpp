#include "point.h"

#include <cmath>

Point::Point()
{
	x = 0;
	y = 0;
}

Point::Point(int p)
{
	x = p;
	y = p;
}

Point::Point(int x, int y)
{
	this -> x = x;
	this -> y = y;
}

Point::Point(const Point &point)
{
	x = point.x;
	y = point.y;
}

Point Point::getDirection(Direction dir)
{
	if     (dir == DIRECTION_EAST)  return Point(1, 0);
	else if(dir == DIRECTION_SOUTH) return Point(0, -1);
	else if(dir == DIRECTION_WEST)  return Point(-1, 0);
	else                            return Point(0, 1);
}

const Point Point::operator+(const Point &rhs) const
{
	return Point(x + rhs.x, y + rhs.y);
}

const Point Point::operator-(const Point &rhs) const
{
	return Point(x - rhs.x, y - rhs.y);
}

const Point Point::operator*(const Point &rhs) const
{
	return Point(x * rhs.x, y * rhs.y);
}

const Point Point::operator*(int rhs) const
{
	return Point(x * rhs, y * rhs);
}

const Point Point::operator/(const Point &rhs) const
{
	return Point(x / rhs.x, y / rhs.y);
}

const Point Point::operator/(int rhs) const
{
	return Point(x / rhs, y / rhs);
}

const Point Point::operator-() const
{
	return Point(-x, -y);
}

Point& Point::operator+=(const Point &rhs)
{
	x += rhs.x;
	y += rhs.y;
	return *this;
}

Point& Point::operator-=(const Point &rhs)
{
	x -= rhs.x;
	y -= rhs.y;
	return *this;
}

Point& Point::operator*=(const Point &rhs)
{
	x *= rhs.x;
	y *= rhs.y;
	return *this;
}

Point& Point::operator*=(int rhs)
{
	x *= rhs;
	y *= rhs;
	return *this;
}

Point& Point::operator/=(const Point &rhs)
{
	x /= rhs.x;
	y /= rhs.y;
	return *this;
}

Point& Point::operator/=(int rhs)
{
	x /= rhs;
	y /= rhs;
	return *this;
}

float Point::length() const
{
	return sqrt(x * x + y * y);
}

int Point::manhattan() const
{
	return fabs(x) + fabs(y);
}
