#pragma once

#include "point.h"
#include "color4.h"

class Image;

class Doorway
{
public:
	Point p1, p2, p3, p4;				// vertices for either side of doorway
	Point p5, p6;						// vertices for closed door that obstructs the doorway
	bool open;							// is the door open?
	bool doorframe;						// do we care about drawing a doorframe?

	Doorway(const Point &pos, const Point &normal, int pixelsPerBlock, int wallInnerOffset, bool open);

	void draw(Image *image, const Color4 &color);
};
