#pragma once

#include "point.h"
#include "block.h"
#include "doorway.h"
#include "wall.h"
#include "corner.h"
#include "color4.h"

#include <stdint.h>
#include <vector>

// How it works:
// At its simplest, the floor layout environment generator works by performing
// random walks on a grid. Each step of the random walk is added to a list
// of "step locations", which are then used as starting points for subsequent
// walks, and so on. After the hallways are built, the step locations are used
// to construct rooms (if rooms are enabled). Doorways and walls are then
// built around the hallway and room grid blocks.

class HallwayGraph;
class Image;

class FloorLayout
{
public:
	// hallways are described by a graph that uses pairs of Blocks for edges
	class BlockEdge
	{
		public:
			BlockEdge(Block *b1, Block *b2);
			Block *b1;
			Block *b2;
	};

	// default colours when drawing the layout to an image; these can be changed
	// using the set******Color() methods below
	static const Color4 DEFAULT_BACKGROUND_COLOR;
	static const Color4 DEFAULT_HALLWAY_BLOCK_COLOR;
	static const Color4 DEFAULT_HALLWAY_INNER_CORNER_BLOCK_COLOR;
	static const Color4 DEFAULT_HALLWAY_OUTER_CORNER_BLOCK_COLOR;
	static const Color4 DEFAULT_ROOM_BLOCK_COLOR;
	static const Color4 DEFAULT_ROOM_INNER_CORNER_BLOCK_COLOR;
	static const Color4 DEFAULT_ROOM_OUTER_CORNER_BLOCK_COLOR;
	static const Color4 DEFAULT_DOORWAY_BLOCK_COLOR;
	static const Color4 DEFAULT_INTERSECTION_BLOCK_COLOR;
	static const Color4 DEFAULT_DOORWAY_COLOR;
	static const Color4 DEFAULT_WALL_COLOR;

	// creates our randomly-generated layout
	FloorLayout(int maxSize,				// max square size of the floor layout
				int pixelsPerBlock,			// resolution of floor layout space
				int wallInnerOffset,		// used to adjust wall thicknesses by bringing walls inwards, in pixels
				int branchLevel,			// controls how many random walks to perform
				int stepSize,				// length of walks---longer values mean longer hallways
				int numStepsPerBranch,		// how many steps to take when we branch from a previous step location
				int spacing,				// block spacing on either side of a path during a random walk
				bool createRooms,			// specifies whether or not to create rooms
				int doorOpenProbability);	// integer percentage probability that a created door is open
	~FloorLayout();

	// allocate, build, and return a 4-channel image of the floor plan---must be freed by the user when done with it
	Image *getImage();

	// set the colours of various floor features that are output in getImage()
	void setBackgroundColor(const Color4 &color);
	void setHallwayBlockColor(const Color4 &color);
	void setHallwayInnerCornerBlockColor(const Color4 &color);
	void setHallwayOuterCornerBlockColor(const Color4 &color);
	void setRoomBlockColor(const Color4 &color);
	void setRoomInnerCornerBlockColor(const Color4 &color);
	void setRoomOuterCornerBlockColor(const Color4 &color);
	void setDoorwayBlockColor(const Color4 &color);
	void setIntersectionBlockColor(const Color4 &color);
	void setDoorwayColor(const Color4 &color);
	void setWallColor(const Color4 &color);

	// used to retrieve info about the structure of the environment; useful in
	// case we want to, e.g., build a 3D environment or have some ground-truth
	// about a simulation, etc.
	void getDoorways(std::vector<Doorway> &doorways);
	void getWalls(std::vector<Wall> &walls);
	void getCorners(std::vector<Corner> &corners);
	void getIntersections(std::vector<Point> &intersections);

private:
	int maxSize;								// max square size of the floor layout
	int pixelsPerBlock;							// resolution of floor layout space
	int wallInnerOffset;						// used to adjust wall thicknesses by bringing walls inwards, in pixels
	int branchLevel;							// controls how many random walks to perform
	int stepSize;								// length of walks---longer values mean longer hallways
	int numStepsPerBranch;						// how many steps to take when we branch from a previous step location
	int spacing;								// block spacing on either side of a path during a random walk
	int doorOpenProbability;					// integer percentage probability that a created door is open

	Block **blocks;								// occupancy grid informing us of the structure of the entire layout
	HallwayGraph *hallways;						// graph that describes the structure of the hallways
	std::vector<Doorway> doorways;				// list of doorway positions
	std::vector<Wall> walls;					// list of wall end points
	std::vector<Corner> corners;				// list of wall corners
	std::vector<Point> intersections;			// list of intersections, in grid space (not world/image/pixel space)

	Color4 backgroundColor;						// colour of empty grid spaces
	Color4 hallwayBlockColor;					// colour of blocks that belong to hallways
	Color4 hallwayInnerCornerBlockColor;		// colour of blocks that belong to hallways that are also inner corners
	Color4 hallwayOuterCornerBlockColor;		// colour of blocks that belong to hallways that are also inner corners
	Color4 roomBlockColor;						// colour of blocks that belong to rooms
	Color4 roomInnerCornerBlockColor;			// colour of blocks that belong to rooms that are also inner corners
	Color4 roomOuterCornerBlockColor;			// colour of blocks that belong to rooms that are also outer corners
	Color4 doorwayBlockColor;					// colour of blocks that contain a doorway
	Color4 intersectionBlockColor;				// colour of blocks that represent intersections
	Color4 doorwayColor;						// colour of closed doors
	Color4 wallColor;							// colour of walls (including corners and doorframes)

	void allocateOccupancyGrid();
	void deallocateOccupancyGrid();

	void buildHallways();
	void buildRooms();
	void buildWalls();
	void findCorners();

	void randomHallwayWalk(const Point &start, int numSteps, int stepSize, int minManDist);
	void fillHallwayOccupancyGrid();

	bool isCornerPiece(const Point &p, Corner::Type &cornerType, Corner::Orientation &cornerOrientation);
	bool areBlocksNotOfType(const Point &p1, const Point &p2, int padding, Block::Type type);
	void fillOccupancyGrid(const Point &p1, const Point &p2, Block::Type type);
};
