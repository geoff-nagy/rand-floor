#include "doorway.h"
#include "point.h"
#include "image.h"
#include "color4.h"

#include <cmath>

Doorway::Doorway(const Point &pos, const Point &normal, int pixelsPerBlock, int wallInnerOffset, bool open)
{
	this -> open = open;

	int halfPixelsPerBlock = pixelsPerBlock / 2;
	Point normalSide(normal.y, normal.x);								// compute vector orthogonal to the normal
	Point normalSideTimesHPPB = normalSide * halfPixelsPerBlock;		// opposing doorway vertices
	Point normalTimesWIO = normal * wallInnerOffset;					// vertices along direction of entry

	// compute closed door vertices
	p5 = pos + normalSideTimesHPPB;
	p6 = pos - normalSideTimesHPPB;

	// compute the vertices of opposing doorframe ends, if the walls have non-zero thickness
	doorframe = wallInnerOffset > 0;
	if(doorframe)
	{
		p1 = p5 + normalTimesWIO;
		p2 = p5 - normalTimesWIO;
		p3 = p6 + normalTimesWIO;
		p4 = p6 - normalTimesWIO;
	}
}

void Doorway::draw(Image *image, const Color4 &color)
{
	// show doorframe only if the walls have non-zero thickness
	if(doorframe)
	{
		image -> line(p1.x, p1.y, p2.x, p2.y, color.r, color.g, color.b, color.a);
		image -> line(p3.x, p3.y, p4.x, p4.y, color.r, color.g, color.b, color.a);
	}

	// show door only if the door is closed
	if(!open)
	{
		image -> line(p5.x, p5.y, p6.x, p6.y, color.r, color.g, color.b, color.a);
	}
}
