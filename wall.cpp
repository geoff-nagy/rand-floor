#include "wall.h"
#include "point.h"
#include "image.h"
#include "color4.h"

Wall::Wall(const Point &pos, const Point &p1, const Point &p2)
{
	this -> pos = pos;
	this -> p1 = p1;
	this -> p2 = p2;
}

void Wall::draw(Image *image, const Color4 &color)
{
	image -> line(p1.x, p1.y, p2.x, p2.y, color.r, color.g, color.b, color.a);
}
