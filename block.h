#pragma once

#include "corner.h"
#include "point.h"

// our occupancy grid is made up of Blocks, which is just a position and a block type
class Block
{
public:
	// a block can be empty, part of a hallway, or part of a room
	typedef enum TYPE
	{
		BLOCK_TYPE_EMPTY = 0,
		BLOCK_TYPE_HALLWAY,
		BLOCK_TYPE_ROOM
	} Type;

	Point pos;							// index in the occupancy grid, rather than pixel position in the resulting image
	Type type;							// either unused, hallway, or room
	bool doorway;						// could also be marked as a doorway entrance/exit
	bool intersection;					// is this block an intersection (i.e., leads to a room or hallway direction change)?
	Corner::Type cornerType;			// corner status of block (none, inner, or outer)

	Block();
	Block(Point pos, Type type);
};
